import { createRouter, createWebHistory } from "vue-router";
import { useServerStore } from "../store/server";
import CampsView from "../views/CampsView.vue";

const ServerView = () => import("../views/ServerView.vue")
const CampEditView = () => import("../views/CampEditView.vue")

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Camps",
      component: CampsView,
    },
    {
        path: "/server",
        name: "Server",
        component: ServerView,
    },
    {
        path: "/edit/camp/:uuid",
        name: "CampEdit",
        component: CampEditView,
    },
  ],
});

/*
router.beforeEach(async (to, from) => {
    const serverStore = useServerStore();

    if (!serverStore.hasUrlAndKey() && to.name !== "Server") {
        // redirect to server details page
        return { name: "Server"}
    }
})
*/

export default router;
