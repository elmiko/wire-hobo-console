import { createPinia } from "pinia";

// create the root store
const store = createPinia();

export default store;
