import { ref } from "vue"
import type { Ref } from "vue"
import { defineStore } from "pinia"

export enum ServerStatus {
  Unconfigured, // no credentials
  Configured, // credentials added or updated
  Connecting, // in the process of connecting to server, no response yet
  Connected, // got a response from the server
  Errored, // uh oh, something happened
}

export interface ServerState {
    url: Ref<string>
    apikey: Ref<string>
    status: Ref<ServerStatus>

    hasUrlAndKey(): boolean
    isStatus(arg0: ServerStatus): boolean
    setStatus(arg0: ServerStatus): void
    setUrlAndKey(arg0: string, arg1: string): void
}

export const useServerStore = defineStore('server', (): ServerState => {
    const url = ref("")
    const apikey = ref("")
    const status = ref(ServerStatus.Unconfigured)

    let lsUrl = localStorage.getItem("wirehobo-url")
    let lsKey = localStorage.getItem("wirehobo-key")

    if (lsUrl !== null) {
        url.value = lsUrl
    }
    if (lsKey !== null) {
        apikey.value = lsKey
    }
    if (url.value !== "" && apikey.value !== "") {
        status.value = ServerStatus.Configured
    }

    function hasUrlAndKey(): boolean {
        return url.value.length > 0 && apikey.value.length > 0;
    }

    function isStatus(desired: ServerStatus): boolean {
        return status.value === desired
    }

    function setStatus(newStatus: ServerStatus) {
        status.value = newStatus
    }

    function setUrlAndKey(newUrl: string, newKey: string) {
        url.value = newUrl;
        localStorage.setItem("wirehobo-url", newUrl)
        apikey.value = newKey;
        localStorage.setItem("wirehobo-key", newKey)
        setStatus(ServerStatus.Configured)
    }

    return {
        url,
        apikey,
        status,

        hasUrlAndKey,
        isStatus,
        setStatus,
        setUrlAndKey,
    }
})
