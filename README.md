# wire hobo console

a web console for interacting with a [wire hobo server](https://gitlab.com/elmiko/wire-hobo-server)
to enable a more convenient administration of an instance.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
