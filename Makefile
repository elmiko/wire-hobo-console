.PHONY: dev image

# run the development server
dev:
	npm run dev

image:
	podman build -f Containerfile -t wire-hobo-console .
